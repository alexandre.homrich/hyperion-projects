Notes for building on COMET
---------------------------

The examples below assume the home directory is `/home/dsd`.

- Modify the `extra-lib-dirs` and `extra-include-dirs` lines in
  `stack.yaml` to point to `/home/dsd/lib` and `/home/dsd/include`

- Download a recent version of [gmp](https://gmplib.org/). Run

      ./configure --prefix=/home/dsd
      make
      make check
      make install

- Download a recent version (>=4.0.0) of [MPFR](https://www.mpfr.org/mpfr-current/#download). Run

      ./configure --prefix=/home/dsd --with-gmp-lib=/home/dsd/lib --with-gmp-include=/home/dsd/include
      make
      make check
      make install

- blas and lapack are needed. To install these on comet, I had to
  download [OpenBLAS](https://github.com/xianyi/OpenBLAS).

      CC=gcc make
      CC=gcc make PREFIX=/home/dsd install

  The `CC=gcc` is because OpenBLAS uses CC=cc by default, and Comet
  sets cc to an old version of gcc. Now we need to symlink the
  openblas shared library to the right paths:

      cd /home/dsd/lib
      ln -s libopenblas.so libblas.so
      ln -s libopenblas.so liblapack.so

- on COMET with ghc-8.2.2, there is some kind of linking problem with
  pthread. You must modify the C compilation flags to make things link
  correctly.

      emacs ~/.stack/programs/x86_64-linux/ghc-gmp4-8.2.2/lib/ghc-8.2.2/settings

  and change the following lines

      ("C compiler flags", " -std=gnu99 -fno-stack-protector"),
      =======>
      ("C compiler flags", " -std=gnu99 -fno-stack-protector -pthread"),

      ("C compiler link flags", " -fuse-ld=bfd"),
      =======>
      ("C compiler link flags", " -fuse-ld=bfd -pthread"),

      ("ld flags", ""),
      =======>
      ("ld flags", "-pthread"),

  (I'm not sure whether all of those lines need to be changed, but this is sufficient.)
  
- Now `stack build` should work.

