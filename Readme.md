About 
-----

`hyperion-projects` is an example haskell program for numerical
conformal bootstrap computations. It includes DSD's O(2) model project
from 2018-2019, along with some other numerical bootstrap
experiments. It uses the
[hyperion](https://github.com/davidsd/hyperion/) cluster management
library and the [hyperion-bootstrap](https://gitlab.com/davidsd/hyperion-bootstrap) library.

Note: as of October 2019, this is everyday working code, and it may change on short-notice.

Usage
-----

- Before compiling, you must create `src/Config.hs` and prepare the appropriate
  scripts so that `hyperion-projects` can use `sdpb` and other programs on your machine.
  The recommended way to do this is:

      cp -r config/caltech-hpc-dsd config/my-special-machine
      # edit 'config/my-special-machine/Config.hs' as necessary
      # edit the scripts in 'config/my-special-machine' as necessary
      cd src
      ln -s ../config/my-special-machine/Config.hs Config.hs

- To test, try

      ./hyp IsingSigEpsAllowed_test_nmax6
  This will run two computations requiring 6 cores each. The computations test
  feasibility of two points in dimension space (Delta_sigma, Delta_epsilon) for
  mixed correlators of sigma and epsilon in the 3d Ising model. One point is
  allowed (primal feasible) and the other is disallowed (dual feasible).
  
  The output of `./hyp` is printed to a file `nohup.out`, and that includes a path
  to a more detailed log file which you can inspect to monitor the computation.
  
  The computation "IsingSigEpsAllowed_test_nmax6" is defined in the file
  `Projects/OPEScans2019.hs`, if you want to see its definition.

Installation notes
-----

- This repository uses git submodules, so to clone the repository, use `git clone --recursive git@gitlab.com:davidsd/hyperion-projects.git`.
	If you have already cloned the repository without the submodules, use `git update --init --recursive`.

- If you want to use Delaunay search, you need the [qhull](http://www.qhull.org/download/) package. It's probably best to follow the instructions
	for installation using `cmake`.

- To compile the executable, you will need to run `stack build`, but some clusters might have memory problems, so it's advised to run `stack build -j1`
	on a compute node as necessary. For reference, Yale's Grace cluster took this command: `srun -N 1 -t 04:00:00 --mem=220G stack build -j1`.

- One of the dependencies needs a libblas.o and liblapack.o. If this is causing compile problems and your cluster uses OpenBLAS or something similar, run something like
	`ln -s /path/to/lib/libopenblas.o /another/path/to/lib/libblas.o` and `ln -s /path/to/lib/libopenblas.o /another/path/to/lib/liblapack.o`,
	and add `/another/path/to/lib` to the extra-lib-dirs of `stack.yaml`.

Other comments
-----

- Configuration is compiled into the program for the following reason:
  Many copies of the executable are run on different machines over the
  course of a computation. If the configuration were read when the
  program started up, one could have different instances of the program
  with different configurations, all communicating with each other.
