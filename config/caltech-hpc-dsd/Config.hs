-- To use, create a symlink 'app/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}

module Config where

import Hyperion.Bootstrap.Main (HyperionBootstrapConfig (..))

config :: HyperionBootstrapConfig
config = HyperionBootstrapConfig
  { -- Email address for cluster notifications
    emailAddr = Just "dsd@caltech.edu"

    -- Working directory for data files
  , dataDir = "/central/groups/dssimmon/dsd/hyperion3/data"

    -- Save logs to this directory
  , logDir = "/central/groups/dssimmon/dsd/hyperion3/logs"

    -- Directory for databases used during computation
  , databaseDir = "/central/groups/dssimmon/dsd/hyperion3/databases"

    -- Hyperion makes a (temporary) copy of its executable in order to run
    -- remote copies of itself. Store executables in this directory
  , execDir = "/central/groups/dssimmon/dsd/hyperion3/executables"

    -- Directory for SLURM jobs
  , jobDir = "/central/groups/dssimmon/dsd/hyperion3/jobs"

    -- Path to a script that does srun sdpb, after loading
    -- appropriate modules and setting environment variables
  , srunSdpbExecutable = "/home/dssimmon/projects/hyperion-projects/config/caltech-hpc-dsd/srun_sdpb.sh"

    -- Path to a script that runs pvm2sdp, after loading
    -- appropriate modules and setting environment variables
  , srunPvm2sdpExecutable = "/home/dssimmon/projects/hyperion-projects/config/caltech-hpc-dsd/srun_pvm2sdp.sh"

    -- Path to a script that runs sdpb (without srun), after loading
    -- appropriate modules and setting environment variables
  , sdpbExecutable = "/home/dssimmon/projects/hyperion-projects/config/caltech-hpc-dsd/sdpb.sh"

    -- Path to a script that runs pvm2sdp (without srun), after loading
    -- appropriate modules and setting environment variables
  , pvm2sdpExecutable = "/home/dssimmon/projects/hyperion-projects/config/caltech-hpc-dsd/pvm2sdp.sh"

    -- Path to a script that runs scalar_blocks, after loading appropriate
    -- modules and setting environment variables
  , scalarBlocksExecutable = "/home/dssimmon/projects/hyperion-projects/config/caltech-hpc-dsd/scalar_blocks.sh"

    -- Path to the qdelaunay executable (needed for Delaunay triangulation searches)
  , qdelaunayExecutable = "/usr/bin/qdelaunay"

    -- Command to use to run the hyperion program. If no option is
    -- specified, hyperion will copy its own executable to a safe place and
    -- run that. Default: Nothing
  , hyperionCommand = Nothing

    -- Default SLURM queue (partition) to submit jobs to. Default: Nothing
  , defaultSlurmPartition = Nothing

    -- Default command for running ssh
  , sshRunCommand = Nothing
  }
