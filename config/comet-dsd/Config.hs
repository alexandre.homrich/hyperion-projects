-- To use, create a symlink 'app/Config.hs' pointing to this file

{-# LANGUAGE OverloadedStrings #-}

module Config where

import Hyperion.Bootstrap.Main (HyperionBootstrapConfig (..))

config :: HyperionBootstrapConfig
config = HyperionBootstrapConfig
  { -- Email address for cluster notifications
    emailAddr = Just "dsd@caltech.edu"

    -- Working directory for data files
    -- dataDir = /oasis/projects/nsf/yun124/dsd/hyperion/data
  , dataDir = "/oasis/scratch/comet/dsd/temp_project/hyperion/data"

    -- Save logs to this directory
  , logDir = "/home/dsd/hyperion-data/logs"

    -- Save logs to this directory
  , databaseDir = "/home/dsd/hyperion-data/databases"

    -- Hyperion makes a (temporary) copy of its executable in order to run
    -- remote copies of itself. Store executables in this directory
  , execDir = "/home/dsd/hyperion-data/executables"

    -- Directory for SLURM jobs
  , jobDir = "/home/dsd/hyperion-data/jobs"

    -- Path to a script that does srun sdpb, after loading
    -- appropriate modules and setting environment variables
  , srunSdpbExecutable = "/home/dsd/projects/hyperion-projects/config/comet-dsd/srun_sdpb.sh"

    -- Path to a script that runs pvm2sdp, after loading
    -- appropriate modules and setting environment variables
  , srunPvm2sdpExecutable = "/home/dsd/projects/hyperion-projects/config/comet-dsd/srun_pvm2sdp.sh"

    -- Path to a script that runs sdpb (without srun), after loading
    -- appropriate modules and setting environment variables
  , sdpbExecutable = "/home/dsd/projects/hyperion-projects/config/comet-dsd/sdpb.sh"

    -- Path to a script that runs pvm2sdp (without srun), after loading
    -- appropriate modules and setting environment variables
  , pvm2sdpExecutable = "/home/dsd/projects/hyperion-projects/config/comet-dsd/pvm2sdp.sh"

    -- Path to a script that runs scalar_blocks, after loading appropriate
    -- modules and setting environment variables
  , scalarBlocksExecutable = "/home/dsd/projects/hyperion-projects/config/comet-dsd/scalar_blocks.sh"

    -- Path to the qdelaunay executable (needed for Delaunay triangulation searches)
  , qdelaunayExecutable = "/home/dsd/src/qhull-2015.2/bin/qdelaunay"

    -- Path to a script that runs tiptop
  , tiptopExecutable = "/home/dsd/projects/hyperion-projects/config/comet-dsd/tiptop.sh"

    -- Command to use to run the hyperion program. If no option is
    -- specified, hyperion will copy its own executable to a safe place and
    -- run that.
  , hyperionCommand = Nothing

    -- Default SLURM queue (partition) to submit jobs to.
  , defaultSlurmPartition = Just "compute"

    -- Default command for running ssh
  , sshRunCommand = Nothing
  }
