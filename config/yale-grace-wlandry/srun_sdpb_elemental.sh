#!/bin/bash

ssh 127.0.0.1 /home/fas/poland/wl424/project/hyperion/runs/srun_sdpb_elemental.sh -n $SLURM_NTASKS_PER_NODE /usr/bin/time /home/fas/poland/wl424/project/install/bin/sdpb --procsPerNode=$SLURM_NTASKS_PER_NODE "$@"
