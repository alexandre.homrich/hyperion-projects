#!/bin/sh

# make sure you run this on the host hard-coded in Config.hs
numactl --interleave=all mongod --dbpath ${HOME}/mongodb/data/db --logpath ${HOME}/mongodb/mongodb.log &
