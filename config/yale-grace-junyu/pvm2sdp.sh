#!/bin/bash

echo "Hello world!"
module load Langs/GCC/5.2.0 Libs/Boost Libs/OpenBLAS MPI/OpenMPI/2.1.1-gcc Tools/Cmake Libs/GMP Libs/MPFR Libs/MPC
export OPENBLAS_NUM_THREADS=1
mpirun /home/fas/poland/wl424/project/install/bin/pvm2sdp $@

