#!/bin/bash

module load Langs/GCC/5.2.0 Libs/Boost Libs/OpenBLAS MPI/OpenMPI/2.1.1-gcc Tools/Cmake Libs/GMP Libs/MPFR Libs/MPC
export OPENBLAS_NUM_THREADS=1
mpirun /usr/bin/time /home/fas/poland/wl424/project/install/bin/sdpb --procsPerNode=$SLURM_NTASKS_PER_NODE $@
