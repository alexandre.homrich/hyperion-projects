module Main where

import           Config                         (config)
import           Hyperion.Bootstrap.Main        (hyperionBootstrapMain,
                                                 tryAllPrograms)
import qualified Projects.IsingSigEpsTest2020
import qualified Projects.FourFermions3dTest2020

main :: IO ()
main = hyperionBootstrapMain config $
  tryAllPrograms
  [ Projects.IsingSigEpsTest2020.boundsProgram
  , Projects.FourFermions3dTest2020.boundsProgram
  ]
