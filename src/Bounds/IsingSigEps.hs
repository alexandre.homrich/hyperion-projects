{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.IsingSigEps
  ( module Exports
  ) where

import           Control.Monad.IO.Class          (liftIO)
import           Hyperion                        (Job, remoteEval, remoteFnIO)
import           Hyperion.Bootstrap.CFTBound     (blockDir, writeSDPPart)
import           Hyperion.Bootstrap.CFTBound     (RemoteWriteSDP (..),
                                                  SDPFetchBuildConfig (..),
                                                  ToSDP (..))
import qualified SDPB.Blocks.ScalarBlocks        as SB
import           SDPB.Blocks.ScalarBlocks.Build  (scalarBlockBuildLink)
import           SDPB.Bounds.Example.IsingSigEps as Exports
import           SDPB.Build.BuildLink            (SomeBuildChain (..), noDeps)
import           SDPB.Build.Fetches              (FetchConfig (..))

instance RemoteWriteSDP Job IsingSigEps where
  remoteWriteSDPPart = remoteEval (static (remoteFnIO writeSDPPart))

instance ToSDP IsingSigEps where
  type SDPFetchKeys IsingSigEps = '[ SB.BlockTableKey ]
  toSDP = isingSigEpsSDP

instance SDPFetchBuildConfig IsingSigEps where
  sdpFetchConfig _ _ cftBoundFiles =
    liftIO . SB.readBlockTable (blockDir cftBoundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $ noDeps $ scalarBlockBuildLink bConfig cftBoundFiles

