{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.FourFermions3d
  ( module Exports
  ) where

import           Control.Monad.IO.Class             (liftIO)
import           Hyperion                           (Job, remoteEval,
                                                     remoteFnIO)
import           Hyperion.Bootstrap.CFTBound        (blockDir, writeSDPPart)
import           Hyperion.Bootstrap.CFTBound        (RemoteWriteSDP (..),
                                                     SDPFetchBuildConfig (..),
                                                     ToSDP (..))
import qualified SDPB.Blocks.Blocks3d               as B3d
import           SDPB.Blocks.Blocks3d.Build         (block3dBuildLink)
import           SDPB.Bounds.Example.FourFermions3d as Exports
import           SDPB.Build.BuildLink               (SomeBuildChain (..),
                                                     noDeps)
import           SDPB.Build.Fetches                 (FetchConfig (..))

instance RemoteWriteSDP Job FourFermions3d where
  remoteWriteSDPPart = remoteEval (static (remoteFnIO writeSDPPart))

instance ToSDP FourFermions3d where
  type SDPFetchKeys FourFermions3d = '[ B3d.BlockTableKey ]
  toSDP = ffSDP

instance SDPFetchBuildConfig FourFermions3d where
  sdpFetchConfig _ _ cftBoundFiles =
    liftIO . B3d.readBlockTable (blockDir cftBoundFiles) :&: FetchNil
  sdpDepBuildChain _ bConfig cftBoundFiles =
    SomeBuildChain $ noDeps $ block3dBuildLink bConfig cftBoundFiles

