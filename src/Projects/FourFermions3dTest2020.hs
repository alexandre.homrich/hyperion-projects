{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.FourFermions3dTest2020 where

import           Bounds.FourFermions3d                       (FourFermions3d (..))
import qualified Bounds.FourFermions3d                       as FF
import           Control.Monad.Catch                         (throwM)
import           Control.Monad.IO.Class                      (liftIO)
import           Control.Monad.Reader                        (asks, local)
import           Data.Aeson                                  (ToJSON)
import           Data.Binary                                 (Binary)
import           Data.Text                                   (Text)
import           GHC.Generics                                (Generic)
import           Hyperion
import           Hyperion.Bootstrap.BinarySearch             (BinarySearchConfig(..),
                                                              binarySearch)
import           Hyperion.Bootstrap.CFTBound                 (CFTBound (..),
                                                              computeCFTBound,
                                                              computeCFTBoundClean,
                                                              continueSDPBCheckpointed,
                                                              setFixedTimeLimit)
import           Hyperion.Bootstrap.CFTBound.ParamCollection (block3dParamsNmax,
                                                              sdpbParamsNmax,
                                                              jumpFindingParams,
                                                              spinsNmax,
                                                              optimizationParams)
import           Hyperion.Bootstrap.Main                     (UnknownProgram (..))
import qualified Hyperion.Database                           as DB
import qualified Hyperion.Log                                as Log
import qualified Hyperion.Slurm                              as Slurm
import           Hyperion.Util                               (minute)
import           Projects.Defaults                           (defaultBoundConfig)
import qualified SDPB.Blocks.Blocks3d                        as B3d
import           SDPB.Bounds.Spectrum                        (setGap,
                                                              setTwistGap,
                                                              unitarySpectrum)
import           SDPB.Bounds.BoundDirection                  (BoundDirection (..))
import           SDPB.Solver                                 (Params (..))
import qualified SDPB.Solver                                 as SDPB


-- TODO: We have lots of duplicated versions of this function because
-- it contains a static pointer. I guess we need to create a typeclass
-- that can hold the static pointer.
remoteComputeFourFermions3dBound :: CFTBound Int FourFermions3d -> Cluster SDPB.Output
remoteComputeFourFermions3dBound cftBound = do
  workDir <- newWorkDir cftBound
  jobTime <- asks (Slurm.time . clusterJobOptions)
  result <-
    continueSDPBCheckpointed (workDir, solverParams cftBound) $
    remoteEvalJob (static (remoteFnJob compute)) (jobTime, cftBound, workDir)
  Log.info "Computed" (cftBound, result)
  DB.insert (DB.KeyValMap "computations") cftBound result
  return result
  where
    compute (jobTime, cftBound', workDir') = do
      -- We ask SDPB to terminate within 90% of the jobTime. This
      -- should ensure sufficiently prompt exit as long as an
      -- iteration doesn't take 10% or more of the jobTime.
      solverParams' <- liftIO $ setFixedTimeLimit (0.9*jobTime) (solverParams cftBound')
      computeCFTBoundClean cftBound' { solverParams = solverParams' } workDir'

data FourFermionBinarySearch = FourFermionBinarySearch
  { ffbs_bound     :: CFTBound Int FourFermions3d
  , ffbs_config    :: BinarySearchConfig Rational
  , ffbs_gapSector :: (Int, B3d.Parity)
  } deriving (Show, Generic, Binary, ToJSON)

remoteFourFermions3dBinarySearch :: FourFermionBinarySearch -> Cluster Rational
remoteFourFermions3dBinarySearch ffbs = do
  workDir <- newWorkDir ffbs
  jobTime <- asks (Slurm.time . clusterJobOptions)
  result  <- remoteEvalJob (static (remoteFnJob compute)) (jobTime, ffbs, workDir)
  Log.info "Computed" (ffbs, result)
  DB.insert (DB.KeyValMap "binary_searches") ffbs result
  return result
  where
    compute (jobTime, ffbs', workDir') = do
      let cftBound = ffbs_bound ffbs'
      -- We ask SDPB to terminate within 90% of the jobTime. This
      -- should ensure sufficiently prompt exit as long as an
      -- iteration doesn't take 10% or more of the jobTime.
      solverParams' <- liftIO $ setFixedTimeLimit (0.9*jobTime) (solverParams cftBound)
      let
        testGap gap = do
            let
              gappedSpectrum = setGap (ffbs_gapSector ffbs') gap (spectrum (bound cftBound))
              cftBound' = cftBound
                { bound = (bound cftBound) { spectrum = gappedSpectrum }
                , solverParams = solverParams'
                }
            result <- computeCFTBound cftBound' { solverParams = solverParams' } workDir'
            Log.info "Binary search step" (cftBound', gap, result)
            DB.insert (DB.KeyValMap "computations") cftBound' result
            pure $ not (SDPB.isDualFeasible result)
      binarySearch (ffbs_config ffbs') testGap

fourFermionsDefaultGaps :: Int -> Rational -> FourFermions3d
fourFermionsDefaultGaps nmax dPsi = FourFermions3d
  { spectrum     = setTwistGap 1e-6 $
                   unitarySpectrum
  , objective    = FF.Feasibility
  , blockParams  = block3dParamsNmax nmax
  , spins        = spinsNmax nmax
  , deltaPsi     = dPsi
  }

boundsProgram :: Text -> Cluster ()

boundsProgram "FourFermions3dAllowed_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (remoteComputeFourFermions3dBound . bound)
  [ (1.1, 3.780) -- Should be allowed
  , (1.1, 3.782) -- Should be disallowed
  ]
  where
    nmax = 6
    bound (deltaPsi, deltaS) = CFTBound
      { bound = (fourFermionsDefaultGaps nmax deltaPsi)
        { spectrum     = setGap (0,B3d.ParityEven) deltaS $
                         setTwistGap 1e-6 $
                         unitarySpectrum
        }
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "FourFermions3d_binary_search_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (30*minute)) $
  mapConcurrently_ (remoteFourFermions3dBinarySearch . search)
  [ 1.1 ]
  where
    nmax = 6
    search deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = fourFermionsDefaultGaps nmax deltaPsi
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityEven)
      , ffbs_config = BinarySearchConfig
        { trueValue  = 2*deltaPsi
        , falseValue = 8
        , threshold  = 1e-3
        }
      }

boundsProgram "FourFermions3d_binary_search_plot_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (30*minute)) $
  mapConcurrently_ (remoteFourFermions3dBinarySearch . search)
  [ 1.02, 1.04 .. 1.7 ]
  where
    nmax = 6
    search deltaPsi = FourFermionBinarySearch
      { ffbs_bound = CFTBound
        { bound = fourFermionsDefaultGaps nmax deltaPsi
        , precision = B3d.precision (block3dParamsNmax nmax)
        , solverParams = (jumpFindingParams nmax) { precision = 768 }
        , boundConfig = defaultBoundConfig
        }
      , ffbs_gapSector = (0, B3d.ParityEven)
      , ffbs_config = BinarySearchConfig
        { trueValue  = 2*deltaPsi
        , falseValue = 8
        , threshold  = 1e-3
        }
      }

boundsProgram "FourFermions3dCTBound_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (remoteComputeFourFermions3dBound . bound)
  [ 1.1
  ]
  where
    nmax = 6
    bound deltaPsi = CFTBound
      { bound = (fourFermionsDefaultGaps nmax deltaPsi)
        { objective = FF.StressTensorOPEBound UpperBound }
      , precision = B3d.precision (block3dParamsNmax nmax)
      , solverParams = (optimizationParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram p = throwM (UnknownProgram p)
