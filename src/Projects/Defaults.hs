{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Projects.Defaults where

import           Config                            (config)
import           Hyperion.Bootstrap.CFTBound       (BoundConfig (..))
import           Hyperion.Bootstrap.DelaunaySearch (DelaunayConfig (..),
                                                    SimplexScore (..))
import           Hyperion.Bootstrap.Main           (HyperionBootstrapConfig (..))
import           Hyperion.Bootstrap.TiptopSearch   (TiptopSearchConfig (..))
import qualified QuadraticNet                      as QN
import           System.FilePath.Posix             ((</>))

defaultBoundConfig :: BoundConfig
defaultBoundConfig = BoundConfig{..}
  where HyperionBootstrapConfig{..} = config

defaultDelaunayConfig :: Int -> Int -> DelaunayConfig
defaultDelaunayConfig nThreads nSteps = DelaunayConfig{..}
  where
    HyperionBootstrapConfig{..} = config
    simplexScore = CandidateSeparation

defaultQuadraticNetConfig :: QN.QuadraticNetConfig
defaultQuadraticNetConfig = QN.defaultQuadraticNetConfig
  (sdp2inputExecutable config)
  (sdpbExecutable config)
  (dataDir config </> "quadratic-net")

defaultTiptopSearchConfig :: Int -> Int -> TiptopSearchConfig
defaultTiptopSearchConfig nThreads nSteps = TiptopSearchConfig{..}
  where
    HyperionBootstrapConfig{..} = config
    heightResolution = 268435456
    widthResolution = 1e-30
    jumpResolution = 2

