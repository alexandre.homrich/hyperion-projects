{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.IsingSigEpsTest2020 where

import           Bounds.IsingSigEps                          (ExternalDims (..),
                                                              IsingSigEps (..))
import qualified Bounds.IsingSigEps                          as Ising
import           Control.Concurrent                          (threadDelay)
import           Control.Monad                               (void)
import           Control.Monad.Catch                         (throwM)
import           Control.Monad.IO.Class                      (liftIO)
import           Control.Monad.Reader                        (asks, local)
import           Data.Aeson                                  (FromJSON, ToJSON)
import qualified Data.Map.Strict                             as Map
import           Data.Text                                   (Text)
import           Data.Typeable                               (Typeable)
import           Hyperion
import           Hyperion.Bootstrap.AffineTransform          (AffineTransform (..))
import           Hyperion.Bootstrap.CFTBound                 (CFTBound (..), computeCFTBoundClean,
                                                              continueSDPBCheckpointed,
                                                              setFixedTimeLimit)
import           Hyperion.Bootstrap.CFTBound.ParamCollection (blockParamsNmax,
                                                              jumpFindingParams,
                                                              optimizationParams,
                                                              sdpbParamsNmax,
                                                              spinsNmax)
import           Hyperion.Bootstrap.Main                     (UnknownProgram (..))
import           Hyperion.Bootstrap.OPESearch                (BilinearForms (..),
                                                              OPESearchConfig (..),
                                                              TrackedMap)
import qualified Hyperion.Bootstrap.OPESearch                as OPE
import qualified Hyperion.Database                           as DB
import qualified Hyperion.Log                                as Log
import qualified Hyperion.Slurm                              as Slurm
import           Hyperion.Util                               (minute)
import           Projects.Defaults                           (defaultBoundConfig,
                                                              defaultDelaunayConfig)
import           SDPB.Bounds.BoundDirection                  (BoundDirection (..))

import           Data.Ratio                                  (approxRational)
import           Hyperion.Bootstrap.DelaunaySearch           (delaunaySearchRegionPersistent)
import           Linear.V                                    (V)
import qualified SDPB.Blocks.ScalarBlocks                    as SB
import           SDPB.Bounds.Spectrum                        (setGaps,
                                                              unitarySpectrum)
import           SDPB.Math.Linear.Literal                    (fromV, toV)
import           SDPB.Solver                                 (Params (..))
import qualified SDPB.Solver                                 as SDPB

remoteIsingSigEpsOPESearch
 :: TrackedMap Cluster (CFTBound Int IsingSigEps) FilePath
 -> TrackedMap Cluster (CFTBound Int IsingSigEps) (V 2 Rational)
 -> Rational
 -> Rational
 -> CFTBound Int IsingSigEps
 -> Cluster Bool
remoteIsingSigEpsOPESearch checkpointMap lambdaMap thetaMin thetaMax =
 OPE.remoteOPESearch (static (remoteFnJob isingSigEpsOPESearch)) checkpointMap lambdaMap initialBilinearForms
 where
   initialBilinearForms =
     BilinearForms 1e-16 [(Nothing, OPE.thetaIntervalFormApprox 1e-16 thetaMin thetaMax)]
   isingSigEpsOPESearch = OPE.opeNetSearch $
     OPESearchConfig setLambda Ising.getExternalMat OPE.queryAllowedBoolFunction
   setLambda lambda cftBound = cftBound
     { bound = (bound cftBound)
       { objective = Ising.Feasibility (Just lambda) }
     }

-- This function is identical to the one below, aside from the
-- type. Is there a nice way to avoid duplication?
remoteComputeIsingSigEpsBound :: CFTBound Int IsingSigEps -> Cluster SDPB.Output
remoteComputeIsingSigEpsBound cftBound = do
  workDir <- newWorkDir cftBound
  jobTime <- asks (Slurm.time . clusterJobOptions)
  result <-
    continueSDPBCheckpointed (workDir, solverParams cftBound) $
    remoteEvalJob (static (remoteFnJob compute)) (jobTime, cftBound, workDir)
  Log.info "Computed" (cftBound, result)
  DB.insert (DB.KeyValMap "computations") cftBound result
  return result
  where
    compute (jobTime, cftBound', workDir') = do
      -- We ask SDPB to terminate within 90% of the jobTime. This
      -- should ensure sufficiently prompt exit as long as an
      -- iteration doesn't take 10% or more of the jobTime.
      solverParams' <- liftIO $ setFixedTimeLimit (0.9*jobTime) (solverParams cftBound')
      computeCFTBoundClean cftBound' { solverParams = solverParams' } workDir'

isingDimVector :: CFTBound prec IsingSigEps -> V 2 Rational
isingDimVector CFTBound{ bound = i } =
  toV (deltaSigma (externalDims i), deltaEps (externalDims i))

newIsingCheckpointMap
 :: AffineTransform 2 Rational
 -> Maybe FilePath
 -> Cluster (TrackedMap Cluster (CFTBound Int IsingSigEps) FilePath)
newIsingCheckpointMap affine mCheckpoint =
 liftIO (OPE.affineLocalityMap affine isingDimVector mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap "isingCheckpoints")

newIsingLambdaMap
  :: (FromJSON l, ToJSON l, Typeable l)
  => AffineTransform 2 Rational
  -> Maybe l
  -> Cluster (TrackedMap Cluster (CFTBound Int IsingSigEps) l)
newIsingLambdaMap affine mLambda =
 liftIO (OPE.affineLocalityMap affine isingDimVector mLambda) >>=
 OPE.mkPersistent (DB.KeyValMap "isingLambdas")

thetaVectorApprox :: Double -> Rational -> V 2 Rational
thetaVectorApprox res theta = toV (cos' theta, sin' theta)
  where
    cos' = (`approxRational` res) . cos . fromRational
    sin' = (`approxRational` res) . sin . fromRational

delaunaySearchPoints :: DB.KeyValMap (V n Rational) (Maybe Bool)
delaunaySearchPoints = DB.KeyValMap "delaunaySearchPoints"

isingFeasibleDefaultGaps :: V 2 Rational -> V 2 Rational -> Int -> IsingSigEps
isingFeasibleDefaultGaps deltaExts lambda nmax = IsingSigEps
  { spectrum     = setGaps [ ((0, Ising.Z2Even), 3)
                           , ((0, Ising.Z2Odd),  3)
                           ] unitarySpectrum
  , objective    = Ising.Feasibility (Just lambda)
  , externalDims = Ising.ExternalDims {..}
  , blockParams  = blockParamsNmax nmax
  , spins        = spinsNmax nmax
  }
  where
    (deltaSigma, deltaEps) = fromV deltaExts

isingAffineNmax6 :: AffineTransform 2 Rational
isingAffineNmax6 =  AffineTransform
  { affineShift  = toV (0.517, 1.395)
  , affineLinear = toV ( toV (0.005, 0.035)
                       , toV (0, 0.02)
                       )
  }

boundsProgram :: Text -> Cluster ()

boundsProgram "IsingSigEpsAllowed_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (remoteComputeIsingSigEpsBound . bound)
  [ (toV (0.517075, 1.396575), thetaVectorApprox 1e-16 0.964)
  , (toV (0.518149, 1.412625), thetaVectorApprox 1e-16 0.96926)
  ]
  where
    nmax = 6
    bound (deltaExts, lambda) = CFTBound
      { bound = isingFeasibleDefaultGaps deltaExts lambda nmax
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (sdpbParamsNmax nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "IsingSigEpsCT_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $ void $
  remoteComputeIsingSigEpsBound $
  bound (toV (0.518149, 1.412625), thetaVectorApprox 1e-16 0.96926)
  where
    nmax = 6
    bound (deltaExts, lambda) = CFTBound
      { bound = (isingFeasibleDefaultGaps deltaExts lambda nmax)
        { objective  = Ising.StressTensorOPEBound (Just lambda) UpperBound }
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (optimizationParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "IsingSigEpsAllowed_OPESearch_test_nmax6" =
 local (setJobType (MPIJob 1 4)) $ do
 checkpointMap <- newIsingCheckpointMap affine Nothing
 lambdaMap     <- newIsingLambdaMap affine Nothing
 mapConcurrently_ (remoteIsingSigEpsOPESearch checkpointMap lambdaMap 0.5 1.5 . bound)
   [ (toV (0.517075, 1.396575), OPE.thetaVectorApprox 1e-16 0.964)
   , (toV (0.518149, 1.412625), OPE.thetaVectorApprox 1e-16 1.0)
   ]
 where
   nmax = 6
   affine = isingAffineNmax6
   bound (deltaExts, lambda) = CFTBound
     { bound = isingFeasibleDefaultGaps deltaExts lambda nmax
     , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
     -- | It is very important to use jumpFindingParams when doing an OPESearch
     , solverParams = (jumpFindingParams nmax) { precision = 768 }
     , boundConfig = defaultBoundConfig
     }

boundsProgram "IsingSigEps_Island_OPESearch_nmax6" =
  local (setJobType (MPIJob 1 4)) $ do
  checkpointMap <- newIsingCheckpointMap affine Nothing
  lambdaMap     <- newIsingLambdaMap affine Nothing
  delaunaySearchRegionPersistent delaunaySearchPoints delaunayConfig affine initialPts
    (remoteIsingSigEpsOPESearch checkpointMap lambdaMap 0.5 1.5 . bound)
  where
    nmax = 6
    affine = isingAffineNmax6
    bound deltaExts = CFTBound
      { bound = isingFeasibleDefaultGaps deltaExts lambdaInit nmax
      , precision = SB.precision (blockParamsNmax nmax :: SB.ScalarBlockParams)
      , solverParams = (jumpFindingParams nmax) { precision = 768 }
      , boundConfig = defaultBoundConfig
      }
    delaunayConfig = defaultDelaunayConfig 20 200
    lambdaInit = OPE.thetaVectorApprox 1e-16 0.962
    initialPts = Map.fromList $
      [(p, Just True)  | p <- initialAllowed] ++
      [(p, Just False) | p <- initialDisallowed]
      where
        initialDisallowed = []
        initialAllowed = [toV (0.5181489, 1.412625)]

boundsProgram "sleep" = do
  Log.text "Sleeping for 5 seconds"
  liftIO (threadDelay (5*1000*1000))

boundsProgram p = throwM (UnknownProgram p)
